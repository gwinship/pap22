%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
\documentclass[12pt,a4paper]{article}

\usepackage{../resources/base}
\usepackage{../resources/pap22}
\usepackage{multirow}
\usepackage{isotope}
\usepackage{pdfpages}
\usepackage{mhchem}


\setcounter{table}{2}
\setcounter{versuchnr}{256}
\renewcommand{\versuchname}{Röntgenfluoreszenz}


\begin{document}
%%% Titelseite BEGIN

\thispagestyle{firstpage}
\newgeometry{top=0.5in+\voffset+\headheight}
\protokollheader
\tableofcontents
\restoregeometry

%%% Titelseite END

%%% Messprotokoll BEGIN

\includepdf[addtotoc={1,section,2,Messprotokoll,sec:messprotokoll},frame=true,pages=-]{data/messprotokoll.pdf}

%%% Messprotokoll END

\section{Einleitung}
\label{sec:einleitung}

In Versuch 256 \textit{Röntgenfluoreszenz} bestimmen wir die Rydberg-Energie $E_{R}$ aus der Energie der $K_{\alpha}$- und $K_{\beta}$-Strahlung einer Reihe von Metallen.
Weiterhin nutzen wir die gewonnen Erkenntnisse um die Zusammensetzung unterschiedlicher Legierungen zu untersuchen.

\section{Grundlagen}
\label{sec:grundlagen}

Wenn Röntgenstrahlung auf Materie trifft, kann die Strahlung Elektronen aus den inneren Schalen der Atome schlagen.
Wenn Elektronen der äußeren Schalen die Leerstelle besetzen wird dabei sekundäre Röntgenstrahlung freigesetzt.
Dies wird als Röntgenfluoreszenz bezeichnet.

Durch das Bohrsche Atommodel lässt sich die Energie der Fluoreszenz-Strahlung approximieren.
Beim Übergang von der Schale mit Hauptquantenzahl $n_1$ zu $n_2$ wird die Energie
\begin{equation}
    \Delta E = E_{n_1} - E_{n_2} = chR_{\infty} \left( \frac{(Z-\sigma_{n_1})^2}{n_1^2} - \frac{(Z-\sigma_{n_2})^2}{n_2^2} \right)
\end{equation}
freigesetzt. Wobei $c$ die Lichtgeschwindigkeit, $h$ das Planck'sche Wirkungsquantum, $Z$ die Kernladungszahl und $R_{\infty}$ die Rydberg-Konstante beschreibt.
Die $\sigma_{n_{i}}$ sind Abschirmkonstanten.
Sie korrigieren, dass die Coulomb Anziehung von äußeren Elektronen teilweise durch die inneren Elektronen abgeschirmt wird.
In guter Näherung kann eine mittlere Abschirmkonstante $\sigma_{n_{12}}$ eingeführt werden.
Die Energiedifferenz ist dann beschrieben durch:
\begin{equation}
    \Delta E = E_{n_1} - E_{n_2} = chR_{\infty} (Z-\sigma_{n_{12}})\left( \frac{1}{n_1^2} - \frac{1}{n_2^2} \right).
\end{equation}

Werden die Konstanten in die Rydberg-Energie $E_{R} = hcR_{\infty} \approx \SI{13.6}{eV}$ zusammengefasst, so ergibt sich:
\begin{equation}
    \sqrt{\frac{E}{E_{R}}} = (Z-\sigma_{n_{12}})\sqrt{\left( \frac{1}{n_{1}^2} - \frac{1}{n_2^2} \right)}.
    \label{eq:mosely}
\end{equation}

Dieser Zusammenhang wird auch als Moseleysches Gesetz bezeichnet.
Für die $K_{\alpha}$-Strahlung ist der Übergang von der L-Schale ($n_2 = 2$) in die K-Schale ($n_1 = 1$).
Für nicht zu schwere Kerne ($Z\approx 30$) kann $\sigma_{12} \approx 1$ angenommen werden.
Für den $K_{\alpha}$-Übergang ergibt sich somit

\begin{equation}
    \sqrt{\frac{E}{E_{R}}} = (Z-1)\sqrt{\frac{3}{4}}
\end{equation}

\paragraph{Röntgenenergiedetektor}

Zur Detektion der Fluoreszenzstrahlung wird eine Halbleiterelektrode verwende, die einem in Sperrrichtung betriebenen pn-Übergang entspricht.
Ein n-Halbleiter ist durch eine hohe Anzahl freier beweglicher Ladungsträger charakterisiert.
Ein p-Halbleiter ist dagegen gerade durch eine hohe Anzahl von Fehlstellen charakterisiert, die sich wie positive Ladungsträger verhalten.
Durch die unterschiedlichen Ladungsträgerdichten kommt es am Übergang zur Diffusion.
Die Rekombination am Übergang führt dann zu einer Verarmungszone in der keine freien Ladungsträger vorhanden sind.
Durch die Verschiebung der Ladungsträger in den Halbleitern baut sich ein der Diffusion entgegen gerichtetes elektrisches Feld auf.
Durch anlegen einer zusätzlichen äußere Spannung, wie in Abbildung \ref{fig:halbleiter} dargestellt, kann die Verarmungszone vergrößert werden.

Wenn Röntgenstrahlung auf die Verarmungszone trifft kann dieses unter Aussenden eines Photoelektrons absorbiert werden.
Die entstandene Ladung ist proportional zur Energie des einfallenden Röntgenquants.
Und kann als Spannungs- und Stromimpuls über die äußere Spannung gemessen werden.

Die von dem Röntgendetektor generierten Pulshöhen sind proportional zur Energie der einfallenden Röntgenstrahlung.
Ein Vielkanalanalysator weißt Impulsintervallen ein Kanal zu und zählt wie viele Impulse in diesem Impulsintervall gemessen werden.
Durch Kalibrierung können den Kanälen dann die entsprechenden Energien der Röntgenstrahlung zugewiesen werden.
In unserem Versuchsaufbau verwenden wir dazu die $K_{\alpha}$-Linien von Eisen und Molybdän.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/halbleiter.png}
    \caption{a) p- und n-Halbleiter. b) pn-Übergang. Elektronen diffundieren vom n-Halbleiter in den p-Halbleiter und Löcher vom p-Halbleiter in den n-Halbleiter. c) im Bereich der Grenzschicht rekombinieren Elektronen und Löcher. In diesem Bereich gibt es keine freien Ladungsträger. d) In Sperrrichtung betriebener pn-Übergang.Quelle: Skript}%
    \label{fig:halbleiter}
\end{figure}

\section{Auswertung}
\label{sec:auswertung}

In Abbildung \ref{fig:alpha} und \ref{fig:beta} sind die die Energie der $K_{\alpha}$- bzw. $K_{\beta}$-Strahlung gegen die Kernladungszahl $Z$ aufgetragen. Der Fehler von $\sqrt{E_{\alpha}}$ berechnet sich dabei nach:
\begin{equation}
    \Delta\left( \sqrt{E_{\alpha}} \right) = \frac{1}{2} \frac{\Delta E_{\alpha}}{\sqrt{E_{\alpha}}}
\end{equation}

Der Fehler von $\sqrt{E_{\beta}}$ berechnet sich analog.

Als Fitfunktion wurde das Moseleysche Gesetz aus Gleichung \eqref{eq:mosely} verwendet.
\begin{equation}
    Fit(Z; \sqrt{E_{R}}, \sigma_{12}) = \sqrt{E_{R}} (Z-\sigma_{12})\sqrt{\left( \frac{1}{n_{1}^2} - \frac{1}{n_2^2} \right)}.
\end{equation}

Im Fall der $K_{\alpha}$-Strahlung wurde $n_1 = 1$ und $n_2 = 2$ gesetzt.
Im Fall der $K_{\beta}$-Strahlung wurde $n_1 = 1$ und $n_2 = 3$ gesetzt.

Die erhaltenen optimalen Fitparameter sind in Tabelle \ref{tab:erg} dargestellt.

\begin{table}[htbp]
    \centering
    \caption{Ergebnisse Moseleysches Gesetz}
    \label{tab:erg}
    \begin{tabular}{cS[table-format=1.1(1)]S[table-format=1.2(1)]S[table-format=2.2(2)]}
    \toprule
    K-Linie  & {$\sigma_{12}$} & {$\sqrt{E_{R}}$ [$\sqrt{\si{eV}}$]} & {$E_{R}$ [$\si{eV}$]} \\ \midrule
    $\alpha$ & 1.2(4) & 3.73(4) & 13.96(31) \\
    $\beta$  & 1.6(3) & 3.67(4) & 13.48(26) \\
    \bottomrule
    \end{tabular}
\end{table}

Im Fall der $K_{\alpha}$-Linie weicht der Wert um $1.17\sigma$ von dem Literaturwert  $E_{R} = \SI{13.598}{eV}$ ab.
Im Fall der $K_{\beta}$-Linie weicht der Messwert $0.44\sigma$ vom Literaturwert ab.
Mit einem relativen Fehler von  2.2\% bzw. 1.9\% stimmen die Ergebnisse daher gut mit den Literaturwerten überein.
Die  $\sigma_{12}$ Werte weichen $0.8\sigma$  voneinander ab. Der relative Fehler der beiden Werte ist mit  33\% bzw. 18\% sehr groß.
Die Abschirmkonstante weicht im Fall von $K_{\alpha}$ $0.5\sigma$ von der Abschätzung 1 ab.
Im Fall der $K_{\beta}$-Linie weicht $\sigma_{12}$ $0.67\sigma$ vom Literaturwert 1.8 ab.
Die Werte stimmen in allen Fällen gut mit den Literaturwerten überein.
Insgesamt konnte das Moseleysche Gesetz bestätigt werden.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/alpha.pdf}
    \caption{$\sqrt{E_{\alpha}}$ als Funktion von $Z$}%
    \label{fig:alpha}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/beta.pdf}
    \caption{$\sqrt{E_{\beta}}$ als Funktion von $Z$}%
    \label{fig:beta}
\end{figure}

\subsection{Zusammensetzung der Legierung}%
\label{sub:zusammensetzung_der_legierung}

Durch Vergleich mit Spektren aus der Datenbibliothek konnten die Zusammensetzungen der Legierungen bestimmt werden.
Die Peaks der $K_{\alpha}$- und $K_{\beta}$-Linien der bestimmten Elemente stimmten dabei mit den Peaks des Spektrums der Legierungen überein.
Die Intensität der Peaks lässt eine qualitative Untersuchung der Verhältnisse der verschiedenen Metalle zu.
Die beschrifteten Kennlinien sind den Abbildungen \ref{fig:messing1} bis \ref{fig:gemisch} zu entnehmen.
Die Ergebnisse sind in Tabelle \ref{tab:legierung} festgehalten.

\begin{table}[htbp]
    \centering
    \caption{Zusammensetzung der unterschiedlichen Legierungen}
    \label{tab:legierung}
    \begin{tabular}{ll}
    \toprule
    Stoff & Zusammensetzung \\ \midrule
    Messing (1) & Kupfer, Zink \\
    Messing (2) & Kupfer, Zink, Blei($L$-Linien) \\
    Edelstahl & Eisen, Chrom, Molybdän \\
    Magnet & Eisen, Nickel, Blei($L$-Linien) \\
    Gemisch & Kupfer, Gallium, Germanium, Blei($L$-Linien) \\ \bottomrule
    \end{tabular}
\end{table}

\section{Zusammenfassung und Diskussion}%
\label{sec:zusammenfassung_und_diskussion}
In dem Versuch wurden aus den Messdaten der $K_{\alpha}$- und $K_{\beta}$-Strahlung die Rydberg-Energie $E_{R}$ und die Abschschirmkonstante $\sigma_{12}$ bestimmt.
Die ermittelten Werte stimmen dabei gut mit dem Literaturwert überein.
Insgesamt konnte das Moseleysche Gesetz bestätigt werden.

Der Fehler in den Abschirmungskonstante ist allerdings sehr groß. Dieser könnte durch genauere Messwerte präziser bestimmt werden.
Je mehr Messwerte aufgenommen werden, desto schmaler sind die Peaks, sodass der Fehler in den $E_{\alpha,\beta}$ Werten geringer wird.

Eine wichtige Fehlerquelle ist die Energiekalibrierung.
Durch schlechte Abschätzung der Eichkanäle kann die Skala verschoben oder verzehrt werden.
Da die Skala allerdings nur linear ist, ist der Effekt viel eher systematischer Natur.
Es ist hilfreich Elemente zu wählen, der $E_{\alpha}$ Energien weit auseinanderliegen.
Dies wurde mit Eisen und Molybdän bereits durchgeführt.
Weiterhin könnte durch genauere Bestimmung der Peaks, also mehr Messungen, die Skalar genauer bestimmt werden.

Bei der Untersuchung der Legierungen könnten systematischen Fehler vorliegen, diese müssten allerdings sehr groß sein um das grobe Ergebnis zu verzehren.
Wenn es um Anteile von Spurenelemente geht, so hätte mit mehr Vorsicht vorgegangen werden.
Dies war allerdings nicht Ziel des Versuchs.
Die Röntgenspektroskopie ist daher eine äußerst Präzise Methode um die Komposition von Metallen zu untersuchen.

\pagebreak

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/messing1.jpg}
    \caption{Vielkanalanalyse Messing (1)}%
    \label{fig:messing1}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/messing1.jpg}
    \caption{Vielkanalanalyse Messing (1)}%
    \label{fig:messing1}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/messing2.jpg}
    \caption{Vielkanalanalyse Messing (2)}%
    \label{fig:messing2}
\end{figure}


\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/edelstahl.jpg}
    \caption{Vielkanalanalyse Edelstahl}%
    \label{fig:edelstahl}
\end{figure}


\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/magnet.jpg}
    \caption{Vielkanalanalyse Magnet}%
    \label{fig:magnet}
\end{figure}


\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/gemisch.jpg}
    \caption{Vielkanalanalyse unbekannte Probe}%
    \label{fig:gemisch}
\end{figure}

\pagebreak
%%% Notebook BEGIN

\includepdf[addtotoc={1,section,2,Notebook,notebook},frame=true,pages=-]{data/versuch256.pdf}

%%% Notebook END

\end{document}

%% TODO
