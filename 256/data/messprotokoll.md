---
header-includes:
    - \usepackage{siunitx}
    - \usepackage{hyperref}
    - \usepackage{booktabs}
---

# Messprotokoll Versuch 256
### Felix Borchers

## Geräteliste
- Röntgengerät mit Röntgenröhre (Molybdän-Anode)
- Röntgenenergiedetektor
- Vielkanalanalysator
- Metallproben
- Computer mit Drucker

## Versuchsaufbau

![Versuchsaufbau](versuchsaufbau.png)

## Einstellungen Röntgengerät
- Röhrenspannung: U = 35 kV
- Strom: I = 1 mA
- 512 Kanäle
- negative Pulse
- Verstärkung-Fakrot: -2.5
- Messzeit 180s


## Energiekalibrierung
Wir fitten eine Gaußkurve an den $K_{\alpha}$ Peak von Eisen und Molybdän.
Wir erhalten somit die Peakkanäle und können die Abszisse mit den bekannten Energiewerten von Eisen und Molybdän bestimmen.

- Eisen: Kanal 87; Energie: 6.40 keV
- Molybdän: Kanal 233; Energie 17.48 keV

## Bestimmung der $K_{\alpha}$ und $K_{\beta}$ Linien der Probemetalle

| Element | $K_{\alpha}$ [keV] | $\Delta K_{\alpha}$ [keV] | $K_{\beta}$ [keV] | $\Delta K_{\beta}$ [keV] |
|---------|--------------------|---------------------------|-------------------|--------------------------|
| Fe      | 6.43               | 0.16                      | 7.05              | 0.16                     |
| Mo      | 17.48              | 0.18                      | 19.59             | 0.21                     |
| Zn      | 8.65               | 0.17                      | 9.60              | 0.17                     |
| Ni      | 7.49               | 0.16                      | 8.27              | 0.17                     |
| Cu      | 8.06               | 0.17                      | 8.92              | 0.17                     |
| Zr      | 15.82              | 0.17                      | 17.70             | 0.17                     |
| Ti      | 4.65               | 0.16                      | 5.08              | 0.12                     |
| Ag      | 21.96              | 0.21                      | 24.64             | 0.23                     |

## Bestimmung der $K_{\alpha}$ und $K_{\beta}$ Linien von Messing (1)

- Wir konnten  die $K_{\alpha}$ und $K_{\beta}$-Linien von Kupfer und Zink feststellen.

## Bestimmung der $K_{\alpha}$ und $K_{\beta}$ Linien von Messing (2)

- Wir konnten  die $K_{\alpha}$ und $K_{\beta}$-Linien von Kupfer, Zink sowie die  $L$-Linien von Blei feststellen.

## Bestimmung der $K_{\alpha}$ und $K_{\beta}$ Linien von Edelstahl

- Wir konnten  die $K_{\alpha}$ und $K_{\beta}$-Linien von Eisen, Chrom und Molybdän feststellen.

## Bestimmung der $K_{\alpha}$ und $K_{\beta}$ Linien des Magneten

- Wir konnten  die $K_{\alpha}$ und $K_{\beta}$-Linien von Eisen, Nickel sowie die  $L$-Linien von Blei feststellen.

## Bestimmung der $K_{\alpha}$ und $K_{\beta}$ Linien der unbekannten Probe

- Wir konnten  die $K_{\alpha}$ und $K_{\beta}$-Linien von Kupfer, Gallium und Germanium sowie die  $L$-Linien von Blei feststellen.
