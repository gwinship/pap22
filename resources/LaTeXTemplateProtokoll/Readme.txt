LaTeX Template for simple article documents,
such as bachelor laboratory documents.

Autor: Matthias Pospiech
Contact: matthias@posiech.eu

For general questions on LaTeX please use on of the available public forums
(http://tex.stackexchange.com, https://texwelt.de/wissen/)

