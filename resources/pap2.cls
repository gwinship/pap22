\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{pap1}[2020/09/30 PAP1 LaTeX class]

\LoadClass[12pt]{article}

%%% Font: Latin Modern Roman
\RequirePackage{lmodern}
\RequirePackage[T1]{fontenc}

%%% Encoding
\RequirePackage[utf8]{inputenc}

\RequirePackage{fontspec}
\RequirePackage{polyglossia}
\RequirePackage{pgf}
\RequirePackage{unicode-math}
\RequirePackage{subcaption}
\setdefaultlanguage{german}

%%% Indent at Beginning of Paragraph
\setlength\parindent{0pt}

%%% Margins
\RequirePackage[a4paper]{geometry}
\geometry{top=3.4cm, bottom=2.67cm, left=3.24cm, right=2.64cm}

%%% Title
\newcounter{versuchnr}
\setcounter{versuchnr}{1234}
\newcommand{\tutor}{VORNAME NACHNAME}
\newcommand{\partner}{Vilija de Jonge}
\newcommand{\versuchname}{VERSUCH NAME}
\newcommand{\protokollheader}{%

        \hrule\vspace*{3mm}

	\begin{center}
            \Large{PAP2.2: Wintersemester 2020/2021}\\
	\end{center}

	\vspace*{1mm}

        \noindent Ausarbeitung von Felix Borchers \hfill Versuch: \arabic{versuchnr} \versuchname \\
	\noindent Partner*in: \partner \hfill Datum: \today

	\vspace*{5mm}

	\hrule \vspace*{2mm}
}

%%% Header and Footer
\RequirePackage{lastpage}
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\setlength{\headheight}{18pt}
\cfoot{Seite\ \thepage\ von \pageref{LastPage}}
\lhead{PAP2.2: SS20 Felix Borchers}
\rhead{Versuch \arabic{versuchnr}}

\fancypagestyle{firstpage}
{
   \fancyhf{}
   \renewcommand{\headrulewidth}{0pt}
   \fancyfoot[C]{Seite \thepage\ von \pageref{LastPage}}
}


%%% Hyperref
\RequirePackage{hyperref}
\hypersetup{
	colorlinks,
	linkcolor=black,
	citecolor=black,
	urlcolor=blue
}
%%% Symbols
\RequirePackage{mathtools}
\RequirePackage{amsfonts, amssymb}
\RequirePackage{wasysym}
\RequirePackage[separate-uncertainty=true]{siunitx}
\RequirePackage{pdfpages}
\RequirePackage{booktabs}
\RequirePackage{apacite}

%%% roman numerals
\newcommand*{\rom}[1]{\expandafter\@slowromancap\romannumeral #1@}

%%% pgfplotstable
% \RequirePackage{pgfplotstable}
% \pgfplotsset{compat=1.17}

%%% tikz
\RequirePackage{tikz}
\RequirePackage[oldvoltagedirection, siunitx,european,straight voltages]{circuitikz}
\RequirePackage{standalone}
