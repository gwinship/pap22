f(Hz)  f_err(Hz)        UM(Vss)     UM_err(Vss)
3.347  0.1              1.26        0.1
5.938  0.2              3.24        0.2
8.850  0.3              5.72        0.2
11.76  0.4              8.32        0.3
14.95  0.5              11.1        0.4
