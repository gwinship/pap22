%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
\documentclass{../resources/pap2}

\usepackage{pgf}
\usepackage{subcaption}
\usepackage{fontspec}
\usepackage{polyglossia}

\setcounter{table}{1}
\setcounter{versuchnr}{21}
\renewcommand{\versuchname}{Elektrolyse}

\newcommand{\degree}{\si{\degree}}
\newcommand{\inputPGF}[2]{%
     \scalebox{#1}{\input{#2}}
}

\begin{document}
%%% Titelseite BEGIN

\thispagestyle{firstpage}
\newgeometry{top=0.5in+\voffset+\headheight}
\protokollheader
\tableofcontents
\restoregeometry


%%% Titelseite END

%%% Messprotokoll BEGIN

\includepdf[addtotoc={1,section,2,Messprotokoll,messprotokoll},frame=true,pages=-]{data/messprotokoll.pdf}

%%% Messprotokoll END

\section{Einleitung}
In Versuch 245 messen wir die induzierte Spannung einer rotierenden Flachspule in dem Magnetfeld einer Helmholtzspule. Dabei untersuchen wir das Verhalten unterschiedlicher Drehfrequenzen sowie zeitlich veränderlicher Magnetfelder. Die Erkenntnisse verwenden wir im Anschluss um das Magnetfeld der Erde zu vermessen.

\section{Grundlagen}
Induktion bezeichnet das Phänomen, dass ein sich zeitlich ändernder magnetischer Fluss durch eine geschlossene Leiterschleife eine Spannung induziert.
\begin{equation}
    U_{ind} = - \frac{d}{dt} \phi = - \frac{d}{dt} \int \vec{B}d\vec{A}
    \label{eq:faraday}
\end{equation}
Wobei $U_{ind}$ die induzierte Spannung, $\phi$ der magnetische Fluss und $\vec{B}$ das Magnetfeld durch die Spulenfläche $\vec{A}$ ist.
Das negative Vorzeichen ist durch die Lenz'sche Regel begründet die besagt, dass die durch Induktion entstehenden Ströme, Felder und Kräfte stets den die Induktion einleitenden Vorgang behindern. \footnote{Demtröder, Experimentalphysik 2, 7. Ausgabe (S.120f.)}
\\

Eine Helmholtzspule besteht aus zwei Spulen, die im Abstand  $d = R$ (Helmholtzbedinung) aufgestellt sind.  $R$ ist hier der Radius der Spulen. Für den Fall, dass die Stromrichtung für beide Spulen dieselbe ist, wird im Mittelpunkt zwischen den Spulen ein $B$-Feld
\begin{equation}
    B = \left( \frac{4}{5} \right)^{\frac{3}{2}} \frac{\mu_0 I N}{R}
    \label{eq:helmholtz}
\end{equation}
entlang der Symmetrieachse erzeugt. Diese Näherung gilt nur für den Punkt zwischen den Spulen.\footnote{Demtröder, Experimentalphysik 2, 7. Ausgabe (S.87f.)} Dabei ist $\mu_0 = 4\pi\times 10^{-7} \text{H/m}$ die Permeabilität des Vakuums (welches in diesem Versuch angenommen wird), $I$ die Stromstärke des an die Spulen angelegten Stroms und $N$ die Anzahl der Windungen in einer Spule.
\\

Für den Fall, dass das Magnetfeld homogen und zeitlich konstant ist und die Spule mit einer Frequenz $\omega$ im Zentrum rotiert lässt sich Gleichung \eqref{eq:faraday} in besonders einfacher Form ausdrücken:
\begin{equation}
    U_{ind} = BA_{FS}N_{FS} \omega\sin(\omega t)
    \label{eq:periodische_induktionsspannung}
\end{equation}

Wobei $\alpha$ der Winkel zwischen Flächennormale und Magnetfeld und $N_{FS}$ die Anzahl der Windungen der Flachspule beschreibt.
Diesen Fall erhalten wir später im Fall der Helmholtzspule bei konstanter Spannung.

Im Fall, dass ein Wechselstrom
\begin{equation}
    I(t) = I_0 \cos(\Omega t)
\end{equation}
an die Helmholtzspulen angelegt ist und sich die die Flachspule in einem Winkel $\alpha$ zum Magnetfeld steht ergibt sich aus Gleichung \eqref{eq:faraday} und \eqref{eq:helmholtz}
\begin{equation}
    U_{ind}(t) = - \frac{d}{dt}\left( \left( \frac{4}{5} \right)^{\frac{3}{2}} \frac{\mu_0 I_0 \cos(\Omega t)N_{H}}{R} N_{FS} A_{FS} \cos{\alpha} \right)
\end{equation}
und somit
\begin{equation}
    U_{ind}(t) = \left( \frac{4}{5} \right)^{\frac{3}{2}} \frac{\mu_0 I_0 \Omega \sin(\Omega t) N_{H}}{R} N_{FS} A_{FS} \cos{\alpha}
    \label{eq:helmholtz_ac}
\end{equation}


Im Fall des Erdmagnetfeldes können wir dieses zu jeder Flächennormalen der Spule in einen parallelen (vertikal) und senkrechten (horizontal) Teil aufspalten.
Wählen wir die Flächennormale der Spule parallel zur Flächennormalen der Erdoberfläche $\vec{n}$ so können wir den Inklinationswinkel bestimmen. Der Inklinationswinkel gibt den Winkel zwischen Flächennormale der Erdoberfläche und dem Magnetfeld an
\begin{equation}
    \alpha = \arccos\left( \frac{|\vec{n}\cdot\vec{B}|}{|\vec{B}|} \right) = \arctan\left( \frac{B_v}{B_h} \right)
\end{equation}

\section{Durchführung [per Video]}
\subsection{Induktionsspannung}
Im ersten Versuchsteil messen wir die Abhängigkeit der Induktionsspannung von der Drehfrequenz der Flachspule $f$ im Bereich  $\SI{3}{\hertz}$ bis $\SI{15}{\hertz}$ bei konstantem Spulenstrom an der Helmholtzspule.
\\

Anschließend messen wir die Induktionsspannung $U_{ind}$ bei konstanter Drehfrequenz $f$ und variieren den Spulenstrom an der Helmholtzspule im Bereich $\SI{0.5}{\ampere}$ bis $\SI{5.0}{\ampere}$.

\subsection{Induktionsspannung bei periodischem Feldstrom}
In diesem Versuchsteil schließen wir an die Helmholtzspulen einen Wechselstrom an. Zunächst messen wir die induzierte Spitze-Spitze Spannung in Abhängigkeit des Drehwinkels $\alpha$ zwischen Magnetfeld und Flächennormale der Feldspule.

Zusätzlich messen wir die induzierte Spannung und den Strom für verschiedene Frequenzen $\Omega$ des Wechselstroms.

\subsection{Bestimmung des Erdmagnetfelds}
Im letzten Versuchsteil kompensieren wir nun die Vertikalkomponente des Erdmagnetfelds indem wir einen Gleichstrom an die Helmholtzspulen anlegen, der ein dem Erdmagnetfeld entgegen gerichtetes magnetisches Feld aufbaut. Aus der Nullmessung, der Kompensationsmessung und der Restspannung können wir anschließend die unterschiedlichen Komponenten bestimmen.

\section{Auswertung}
Im Messprotokoll wurde stets die Spitze-Spitze Spannung $U_{SS}$ gemessen. Im folgenden werden wir vor allem die Maximalspannung $U_{\max} = \frac{1}{2} U_{SS}$ brauchen, daher wird überall die Umrechnung zu $U_{\max}$ bereits implizit vorausgesetzt werden.

\subsection{Induktionsgesetz}
In Abbildung \ref{fig:aufgabe1_1} und \ref{fig:aufgabe1_2} sind die Messwerte dargestellt. Sowohl für die Frequenzabhängigkeit als auch für die Spulenstromabhängigkeit wurde eine Gerade gefittet. Die Steigungen der beiden Geraden betragen:
\begin{align}
    \boxed{ c_1 = \SI{0.42(1)}{\volt/\hertz} }\\
    \boxed{ c_2 = \SI{0.65(1)}{\volt/\ampere} }\\
\end{align}

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{figures/aufgabe1_1.jpg}
    \caption{Induktionsgesetz: Frequenzmessung}
    \label{fig:aufgabe1_1}
\end{figure}

\begin{figure}
    \includegraphics[width=\linewidth]{figures/aufgabe1_2.jpg}
    \caption{Induktionsgesetz: Strommessung}
    \label{fig:aufgabe1_2}
\end{figure}



Nach Gleichung \eqref{eq:periodische_induktionsspannung} gilt für die Induktionsspannung
\begin{equation}
    U_{ind}(t) = - B A_{FS}N_{FS} \omega \sin(\omega t) = - B A_{FS}N_{FS} 2\pi f \sin(2\pi f t)
\end{equation}
Wobei $B$ das Magnetfeld der Helmholtzspule, $A_{FS}$ die Fläche der Flachspule und $N_{FS}$ die Anzahl der Windungen der Flachspule sind.

Im Fall $\sin(\omega t) = 1$ gilt $U_{ind}(t) = U_{\max}$ sodass die Steigung $c_1 = 2\pi BA_{FS}N_{FS}f$. Somit können wir das Magnetfeld der Helmholtzspule ausdrücken durch
\begin{equation}
    B = \frac{c_1}{2\pi BA_{FS}N_{FS}} \qquad \Delta B = B \frac{\Delta c_1}{c_1}.
\end{equation}

Das Magnetfeld ergibt sich mit unseren Messungen dann zu:
\begin{equation}
    \boxed{B = \SI{3.98(7)}{\milli\tesla}}
\end{equation}


Der theoretische Wert lässt sich aus der Gleichung für das Magnetfeld im Zentrum der Helmholtzspule, Gleichung \eqref{eq:helmholtz}, bestimmen:
\begin{equation}
    B_{\text{theo}} = \left( \frac{4}{5} \right)^{\frac{3}{2}} \frac{\mu_0 I N_{H}}{R}
\end{equation}

Die Anzahl der Windungen $N_{H} = 124$ und der Radius $R = \SI{147.5}{\milli\meter}$. Somit

\begin{equation}
    \boxed{ B_{\text{theo}} = \SI{3.55(8)}{\milli\tesla}}
\end{equation}

Die beiden Werte stimmen nicht in ihren Fehlerbereichen überein. Die Abweichung
\begin{equation}
    \text{Abweichung} = \frac{\left|B_{\text{theo}} - B\right|}{\sqrt{\Delta B_{\text{theo}}^2 + \Delta B^2}}
\end{equation}

beträgt $4\sigma$. Ebenfalls geht die Gerade 1, anders als von Gleichung \eqref{eq:periodische_induktionsspannung} vorhergesagt, nicht durch den Ursprung ($U(f=0) = \SI{-0.78(5)}{\volt}$). Dies deutet auf einen systematischen Fehler hin.
\\
In Abbildung 2 geht die Gerade nahezu durch den Ursprung. Dies lässt darauf schließen, dass äußere Faktoren eine Rolle spielen.
\\
Eine mögliche Fehlerquelle wäre die Approximation der Helmholtzspule, wobei die Flachspule eine Ausdehnung besitzt und dementsprechend um das Zentrum rotiert.
Ebenfalls stimmt die Helmholtzbedingung für die Spule nur näherungsweise, mit $R = \SI{147.5}{\milli\meter} \approx d = \SI{147}{\milli\meter}$ überein.
Abhängig von der Ausrichtung der Helmholtzspule wird zusätzlich das Magnetfeld der Erde erfasst, welches zum Magnetfeld der Helmholtzspule addiert. Durch eine Eichung könnte dieser Faktor ausgeschlossen werden.

\subsection{Induktionsspannung bei periodischem Feldstrom}
\subsubsection{Winkelabhängigkeit}

Weiterhin untersuchen wir die Induktionsspannung im Fall eines äußeren periodischen Magnetfelds.
Nach Gleichung \eqref{eq:helmholtz_ac} ist die maximale Induktionsspannung gegeben durch:
\begin{equation}
    U_{\max} = B_0 A_{FS} N_{FS} \Omega | \cos{\alpha} |
\end{equation}

Wobei $\Omega = 2\pi\SI {103.1(1)}{\hertz}$ die Winkelgeschwindigkeit, des an die Helmholtzspule angelegten Stroms und $\alpha$ der Winkel zwischen Flächennormalenvektor der Flachspule und der Magnetfeldrichtung der Helmholtzspule.
Die Messdaten sind in Abbildung \ref{fig:aufgabe2_1} abgebildet. Aus dem Fit erhalten wir für $a = B_0 A_{FS}N_{FS}\Omega$
\begin{equation}
    \boxed{a = \SI{3.35(14)}{\volt/ \degree}}
\end{equation}

Damit erhalten wir für das Magnetfeld
\begin{equation}
    B = \frac{a \frac{\pi}{180\degree}}{A_{FS}N_{FS}\Omega}
\end{equation}
\begin{equation}
    \boxed{B = \SI{5.42(3)}{\micro\tesla}}
\end{equation}

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{figures/aufgabe2_1.jpg}
    \caption{Induktionsspannung: Winkelabhängigkeit}
    \label{fig:aufgabe2_1}
\end{figure}



\subsubsection{Frequenzabhängigkeit}
Nun untersuchen wir wie sich das Verhältnis von induzierter Spannung zu angelegter Spannung für verschiedene Frequenzen verhält. In Abbildung \ref{fig:aufgabe2_2} sieht man, dass das Verhältnis zunächst ansteigt und sich dann für höhere Frequenzen stabilisiert.
Der Grund dafür ist, dass die Flachspule neben dem induktiven Widerstand auch einen ohmschen Widerstand besitzt, der für kleinere Frequenzen ins Gewicht fällt.
Das Verhältnis zwischen Spannung $U_{H}$ und Stromstärke $I_{H}$ beträgt für einen rein induktiven Widerstand $Z_{L} = i\omega L$
\begin{equation}
    |U_{H}| = \omega L |I_{H}|
\end{equation}
Da ebenfalls $U_{\max} \propto |I_{H}|$ gilt
\begin{equation}
    \frac{|U_{\max}|}{|U_{H}|} = f(\omega) = const.
\end{equation}

Die Impedanz beträgt allerdings eigentlich
\begin{equation}
Z = R + i\omega L \qquad |Z| = \sqrt{R^2 + \omega^2L^2}.
\label{eq:impedanz}
\end{equation}

Somit gilt

\begin{equation}
    |I_{H}| = \frac{|U_{H}|}{\sqrt{R^2 +\omega^2L^2}}
\end{equation}
Dies erklärt, warum sich bei konstanter Spannung die Stromstärke abfällt.

\subsubsection{Widerstand und Induktivität $L$}

Im folgenden bestimmen wir die Induktivität der Helmholtzspule. In Abbildung \ref{fig:aufgabe2_3} ist dafür der Widerstand (der Absolutbetrag der Impedanz $Z$) gegen die Frequenz $\Omega$ aufgetragen.
Wir Fitten eine Funktion $f(x) = \sqrt{cx^2+d}$. Vergleich mit Gleichung \eqref{eq:impedanz} ergibt
\begin{equation}
    L = \sqrt{c} \qquad \Delta L = \frac{L}{2} \frac{\Delta c}{c}
\end{equation}

Die Induktivität beträgt demnach
\begin{equation}
    \boxed{L = \SI{0.584(1)}{\milli\ohm/\hertz}}
\end{equation}

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{figures/aufgabe2_2.jpg}
    \caption{Verhältnis Induktionsspannung zu angelegter Spannung}
    \label{fig:aufgabe2_2}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{figures/aufgabe2_3.jpg}
    \caption{Impedanz der Helmholtzspule}
    \label{fig:aufgabe2_3}
\end{figure}


\subsection{Bestimmung des Erdmagnetfeldes durch Kompensation}
\subsubsection{Betrag des Erdmagnetfeldes}
Das Magnetfeld der Erde beträgt gemäß Gleichung \eqref{eq:periodische_induktionsspannung}
\begin{equation}
    B = \frac{U_{\max}}{2\pi f N_{FS}A_{FS}} \qquad \Delta B = B \sqrt{\left( \frac{\Delta U_{\max}}{U_{\max}} \right)^2 + \left( \frac{\Delta f}{f} \right)^2 }
\end{equation}

Wobei $U_{\max}$ die maximal Spannung der Flachspule ist.

Der Betrag des Erdmagnetfeldes beträgt somit.

\begin{equation}
    \boxed{B_{E} = \SI{48.0(6)}{\micro\tesla}}
\end{equation}

Der Literaturwert beträgt bei den Koordinaten ($49\degree\;25'$,  $8\degree\;40'$, 114 m  ü.NN:) nach \url{http://www-app3.gfz-potsdam.de/Declinationcalc/declinationcalc.html}
\begin{equation}
    B_{Lit} = \SI{48.7}{\micro\tesla}
\end{equation}

der gemessene Wert weicht somit $1.27\sigma$ von dem Literaturwert ab.

\subsubsection{Vertikal- und Horizontalkomponente des Magnetfeldes}
Die Vertikalkomponente entspricht gerade dem Magnetfeld der Helmholtzspule (s. Gleichung \eqref{eq:helmholtz}) mit Kompensationsstrom $I$.
Die Vertikalkomponente beträgt daher
\begin{equation}
    \boxed{B_{v} = \SI{47.9(8)}{\micro\tesla}}
\end{equation}

Die Horizontalkomponente kann dann aus der verbleibenden Spannung wie zuvor der Betrag des Erdmagnetfelds bestimmt werden.
Die Horizontalkomponente beträgt somit
\begin{equation}
    \boxed{B_{h} = \SI{5.2(4)}{\micro\tesla} }
\end{equation}

Die entsprechenden Literaturwerte sind
\begin{align}
    B_{v}^{(lit)} &= \SI{44.2}{\micro\tesla} \\
    B_{h}^{(lit)} &= \SI{20.4}{\micro\tesla}
\end{align}

Wir sehen, dass beide Werte stark von den Literaturwerten abweichen.


Die Geometrie des Problems erlaubt es uns nun die Inklination $\alpha$ auf drei verschiedene weisen zu bestimmen.
\begin{align}
    \alpha_1 &= \arctan(\frac{B_{v}}{B_{h}}) \qquad &\Delta\alpha &= \frac{1}{1+\left( \frac{B_{v}}{B_{h}} \right)^2} \sqrt{\left( \frac{\Delta B_{v}}{B_{h}} \right)^2 + \left( \frac{B_{v}\Delta B_{h}}{B_{h}^2} \right)^2} \\
    \alpha_2 &= \arcsin(\frac{B_{v}}{B_{E}}) \qquad &\Delta\alpha &= \frac{1}{\sqrt{1-\left( \frac{B_{v}}{B_{E}} \right)^2}} \sqrt{\left( \frac{\Delta B_{v}}{B_{E}} \right)^2 + \left( \frac{B_{v}\Delta B_{E}}{B_{E}^2} \right)^2} \\
    \alpha_3 &= \arccos(\frac{B_{h}}{B_{E}}) \qquad &\Delta\alpha &= \frac{1}{\sqrt{1-\left( \frac{B_{h}}{B_{E}} \right)^2}} \sqrt{\left( \frac{\Delta B_{h}}{B_{E}} \right)^2 + \left( \frac{B_{h}\Delta B_{E}}{B_{E}^2} \right)^2} \\
\end{align}

Damit erhalten wir
\begin{align}
    \alpha_1 &= \SI{83.6(5)}{\degree} \\
    \alpha_2 &= \SI{87.5(8)}{\degree} \\
    \alpha_3 &= \SI{83.8(5)}{\degree} \\
\end{align}

gemittelt erhalten wir
\begin{equation}
    \boxed{\alpha = \SI{85.0(3)}{\degree}}
\end{equation}

Dies weicht sehr stark vom Literaturwert
\begin{equation}
    \alpha_{lit} = \SI{65.18}{\degree}
\end{equation}
ab.

Da der Betrag des Erdmagnetfeld allerdings gut mit dem Literaturwert übereinstimmt ist davon auszugehen, dass die Apparatur nicht richtig ausgerichtet war. Allerdings gilt ebenfalls, dass dies die Vertikalkomponente unberührt lassen sollte. Diese weicht allerdings ebenfalls signifikant vom Literaturwert ab. Gründe hierfür könnten ähnlich zu denen in der ersten Aufgabe sein. Die Apparatur sollte allerdings eigentlich Richtung Norden ausgerichtet sein. Ansonsten könnte die selbe Messung für verschiedene Ausrichtungen vorgenommen werden.

\section{Zusammenfassung und Diskussion}
In diesem Versuch konnten wir das Induktionsgesetz mit einer Feldspule und einer Helmholtzspule überprüfen. Die Vorhersagen stimmten dabei mit der theoretischen Erwartung überein.
Weiterhin ermöglichte und die Messung der Induktionsspannung und der Stromstärke die Bestimmung der Induktivität $L$ zu
\begin{equation}
    \boxed{L = \SI{0.584(1)}{\milli\ohm/\hertz}}
\end{equation}
Die Bestimmung ist dabei mit einem relativen Fehler von 0.17\% sehr genau.

Schwieriger war die Bestimmung des Erdmagnetfeldes. Der gemessene Betrag des Erdmagnetfeldes weicht dabei lediglich $1.27\sigma$ vom Literaturwert ab. Die Vertikalkomponente weicht  $4.6\sigma$ und die Horizontalkomponente  $38\sigma$. Es ist daher davon auszugehen, dass bei  $B_{h}$ weiter Fehler vorliegen. Diese abzuschätzen war uns in diesem Versuch nicht möglich. Mögliche Fehlerquellen wurden in der Auswertung diskutiert. Der Fehler wurde wahrscheinlich unterschätzt.
Aufgrund der starken Abweichung von $B_{h}$ erhalten wir ebenfalls einen stark vom Literaturwert abweichenden Wert für den Inklinationswinkel $\alpha$.
\begin{equation}
    \boxed{\alpha = \SI{85.0(3)}{\degree}, \qquad \alpha_{lit} = \SI{65.18}{\degree}}
\end{equation}


%%% Notebook BEGIN

\includepdf[addtotoc={1,section,2,Notebook,notebook},frame=true,pages=-]{data/versuch245.pdf}

%%% Notebook END

\end{document}

%%% TODO
