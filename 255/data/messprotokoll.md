---
header-includes:
    - \usepackage{siunitx}
    - \usepackage{hyperref}
    - \usepackage{booktabs}
---

# Messprotokoll Versuch 255
### Felix Borchers

## Geräteliste
- Röntgengerät mit Röntgenröhre (Molybdän-Anode)
- Goniometer
- Zählrohr
- LiF-Kristall
- NaCl-Kristall
- Computer mit Drucker
- Leuchtschirm mit CCD-Kamera

## Versuchsaufbau

![Versuchsaufbau](versuchsaufbau.png)

## Messung des Röntgenspektrums mit einem LiF-Kristall
###  Messung Röntgenspektrum
- Röhrenspannung U = 35kV
- Strom I = 1mA
- Messzeit t = 5s
- Winkelschrittweite $\Delta\beta$ = 0.2$\si{\degree}$
- Scanbereich: $3\si{\degree} - 22\si{\degree}$

### Bestimmung $K_{\alpha}$- und $K_{\beta}$-Linie Erster-Ordnung
- Röhrenspannung U = 35kV
- Strom I = 1mA
- Messzeit: t = 20s
- Winkelschrittweite $\Delta\beta$ = 0.1$\si{\degree}$
- Scanbereich: $8.4\si{\degree} - 10.8\si{\degree}$

### Bestimmung $K_{\alpha}$- und $K_{\beta}$-Linie Zweiter-Ordnung
- Röhrenspannung U = 35kV
- Strom I = 1mA
- Messzeit: t = 20s
- Winkelschrittweite $\Delta\beta$ = 0.1$\si{\degree}$
- Scanbereich: $17.4\si{\degree} - 21.2\si{\degree}$

### Zählratenmessung bei festem Winkel
- Röhrenspannung: U = $20\si{kV} - 35\si{kV}$
- Strom: I = 1mA
- Messzeit: t = 20s
- Winkelschrittweite $\Delta\beta$ = 0.1$\si{\degree}$
- Scanbereich: $7.5\si{\degree} - 7.6\si{\degree}$

| Spannung [kV] | Zählrate [1/s] |
|---------------|----------------|
|    20         |       1.30     |
|    21         |       2.10     |
|    22         |       3.20     |
|    23         |       15.20    |
|    24         |       54.95    |
|    25         |       88.85    |
|    26         |       130.2    |
|    27         |       161.0    |
|    28         |       192.7    |
|    29         |       227.6    |
|    30         |       256.9    |
|    31         |       286.1    |
|    32         |       312.4    |
|    33         |       348.0    |
|    34         |       372.7    |
|    35         |       404.8    |

## Messung des Röntgenspektrums mit einem NaCl-Kristall
###  Messung Röntgenspektrum
- Röhrenspannung U = 35kV
- Strom I = 1mA
- Messzeit t = 5s
- Winkelschrittweite $\Delta\beta$ = 0.2$\si{\degree}$
- Scanbereich: $3\si{\degree} - 18\si{\degree}$

## Röntgenaufnahmen
*entfällt*
