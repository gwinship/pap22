%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
\documentclass[12pt,a4paper]{article}

\usepackage{../resources/base}
\usepackage{../resources/pap22}
\usepackage{multirow}
\usepackage{isotope}
\usepackage{pdfpages}
\usepackage{mhchem}


\setcounter{table}{2}
\setcounter{versuchnr}{255}
\renewcommand{\versuchname}{Röntgenspektrometer}


\begin{document}
%%% Titelseite BEGIN

\thispagestyle{firstpage}
\newgeometry{top=0.5in+\voffset+\headheight}
\protokollheader
\tableofcontents
\restoregeometry

%%% Titelseite END

%%% Messprotokoll BEGIN

\includepdf[addtotoc={1,section,2,Messprotokoll,sec:messprotokoll},frame=true,pages=-]{data/messprotokoll.pdf}

%%% Messprotokoll END

\section{Einleitung}
In Versuch 255 untersuchen wir die Röntgenspektra der Kristalle LiF und NaCl.
Wir bestimmen dabei die charakteristischen K-Linien, das Planck'sche Wirkumsquantum $h$, den Netzebenenabstand  $d$ sowie die Avogadro-Zahl.

\section{Grundlagen}
\label{sec:grundlagen}

Eine Röntgenelektrode besteht aus einem evakuierten Glaskolben und zwei Elektroden. An der Kathode werden durch Glühemission freie Elektronen erzeugt, die durch eine Beschleunigungsspannung $U$ Richtung Anode beschleunigt werden.
Die beim Aufprall verlorene Energie wir teilweise in Form von elektromagnetischen Wellen abgestrahlt. Da der Energieverlust beim Abbremsen unterschiedlich groß ist, ensteht ein kontinuierliches Bremsspektrum.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/roentgenroehre.png}
    \caption{Aufbau einer Röntgenröhre Quelle: Skript}%
    \label{fig:roentgenroehre}
\end{figure}

Das Bremsspektrum setzt erst oberhalb einer Grenzwellenlänge $\lambda_{gr}$ ein.
Diese entspricht gerade der Wellenlänge, wenn ein beschleunigtes Elektron beim Aufprall vollständig abgebremst wird.
Es gilt:
\begin{equation}
    E = eU = h\nu_{gr} = h \frac{c}{\lambda_{gr}} \Rightarrow \lambda_{gr} = \frac{hc}{eU}.
    \label{eq:grenz_lambda}
\end{equation}
Mit dem Plank'schen Wirkungsquantum $h$ und der Lichtgeschwindigkeit  $c$.

Außerdem wird für große Beschleunigungsspannungen das kontinuierliche Bremsspektrum noch durch ein diskretes Spektrum überlagert.
Diskrete Linien entstehen, wenn die Energie der aufprallenden Ionen groß genug ist um das ein Atom im Anodenmaterial zu ionisieren.
Wenn ein Atom aus einer äußeren Schicht das entstanden Loch füllt wird die freiwerdende Bindungsenergie in Form eines Röntgenquants abgestrahlt.
Der Energiewert des Übergangs hängt dabei von dem Anodenmaterial und von dem Übergang selber ab.
Erfolgt ein Elektronenübergang auf die innerste Schale (K-Schale), so spricht man von der K-Series.
Beim Übergang L-Schale  $ \rightarrow  $ K-Schale wird $K_{\alpha}$-Strahlung und beim Übergang M-Schale $ \rightarrow $ K-Schale wird $K_{\beta}$-Strahlung freigesetzt.

Wird die Feinstruktur vernachlässigt, so lässt sich die freigesetzte Energie durch das Moseley'sche Gesetz abschätzen.
\begin{equation}
    E_{n \rightarrow m} = hc R_{\infty} (Z-A)^2 \left( \frac{1}{m^2} - \frac{1}{n^2} \right)
\end{equation}
Dabei entspricht $h$ dem Planck'schen Wirkungsquantum,  $c$ der Lichtgeschwindigkeit, $R_{\infty}$ der Rydbergkonstante , $Z$ der Kernladungszahl und  $n$ bzw.  $m$ der jeweiligen Hauptquantenzahl.
Die Größe  $A$ ist eine Abschirmkonstante, die für $K_{\alpha}$-Strahlung in guter Näherung $A\approx 1$ ist. \\

Zur Untersuchung eines Röntgenspektrums bzw. des Aufabus eines Kristalls wird die Bragg-Reflexion  genutzt. Dabei werden Kristalle als Beugungsgitter verwendet.
Die Kristalle bieten sich an, da die Gitterabstände der Atome ähnlich der Wellenlänge von Röntgenstrahlung sind.

Es wird angenommen, dass Strahlung nach dem Reflexionsgesetz an den Atomen reflektiert und dass die Strahlung tief in den Kristall eindringen kann.
In diesem Fall existiert ein Gangunterschied $\Delta s$ für Strahlen, die an der der oberen Schicht und an tieferen Netzebenen reflektieren (s. Abbildung \ref{fig:bragg_refl}).

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/bragg_refl}
    \caption{Bragg-Reflexion}%
    \label{fig:bragg_refl}
\end{figure}

Für den Gangunterschied muss gelten:
\begin{equation}
    \Delta s = 2d\sin\vartheta
\end{equation}

Damit die konstruktive Interferenz zweier Teilbündel entsteht muss der Gangunterschied ein Vielfaches der Wellenlänge $\lambda$ betragen wodurch man die Bragg-Gleichung erhält:
\begin{equation}
    2d\sin\vartheta = n\lambda, \;\; n \in \mathbb{N}
    \label{eq:bragg}
\end{equation}

Die unter dem Winkel $\vartheta$ reflektierte Strahlung hängt also von der Röntgenstrahlung und der Kristallstruktur ab.
Die Bragg-Reflexion eignet sich somit zur Untersuchung Messung des Spektrums einer Röntgenquelle.

Kristalle sind streng periodisch aufgebaut.
Die kleinste sich wiederholende Einheit nennt man Elementarzelle.
Kristalle aus NaCl und LiF besitzen kubische Elementarzellen.
In diesem Fall entspricht die halbe Gitterkonstante $a$ gerade dem Netzebenenabstand  $a = 2d$.

Wenn beachtet wird, dass in der Gitterstruktur manche Atome in mehreren Elementarzellen liegt und diese dementsprechend gewichtet kann man die Gesamtanzahl der Atome einer Elementarzelle $n$ berechnen.
Im Fall von NaCl und LiF folgt $n = 4$.

Für das Volumen einer Elementarzelle gilt
\begin{equation}
    V_{El} = a^3
\end{equation}
Die Avogadro-Konstante lässt sich dann ausdrücken durch
\begin{equation}
    N_{A} = 4 \frac{V_{Mol}}{V} = 4\frac{V_{Mol}}{(2d)^3} = \frac{1}{2}\frac{M_{Mol}}{\rho d^3}.
    \label{eq:avo}
\end{equation}
Mit dem Molgewicht $M_{Mol}$ und der Dichte $\rho$.

\section{Auswertung}
\label{sec:auswertung}

\subsection{Bestimmung der Grenzwellenlänge LiF}%
\label{ssub:bestimmung_der_grenzwellenlange_lif}

Um die Grenzwellenlänge zu bestimmen tragen wir die Zählrate gegen den Winkel auf.
Die Messwerte sind in Abbildung \ref{fig:spektrum_LiF} dargestellt.
Der Fehler der Zählrate ist gegenen durch $\Delta n = \sqrt{n/t}$, mit der Messzeit $t$.
Anschließend fitten wir eine lineare Funktion

\begin{equation}
    Fit(x; m, b) = mx +b
\end{equation}

an die Punkte am kurzwelligen Ende.
Als Fitparameter erhalten wir
\begin{align}
    m &= \SI{473(9)}{Zählrate/Grad} \\
    b &= \SI{-2245(47)}{Zählrate}
\end{align}

Wir bestimmen den Untergrund $n_0$ durch Mittlung der Werte bis einschließlich $\SI{4.8}{\degree}$.
\begin{equation}
    n_0 = \SI{23(3)}{Zählrate}
\end{equation}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/mess_LiF.png}
    \caption{Röntgenspektrum LiF}%
    \label{fig:spektrum_LiF}
\end{figure}


Wir bestimmen den Grenzwinkel $\theta_gr$ nun als Schnittpunkt der gefitteten Gerade mit  $n_0$.
Hierzu verwenden wir die Funktion \texttt{scipy.optimize.root}\footnote{\url{https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.root.html}}.
Den Fehler ermitteln wir durch Bestimmung des Schnittpunkts für die optimalen Parameter und die $1\sigma$ Abweichung von den optimalen Fitparametern.
Die Messdaten sowie die Fitfunktion sind in Abbildung \ref{fig:grenzwinkel_lif} dargestellt.

Als Grenzwinkel erhalten wir somit:
\begin{equation}
    \theta_{gr} = \SI{4.80(5)}{\degree}
\end{equation}

Die Grenzwellenlänge können wir aus Gleichung \eqref{eq:bragg} der Bragg-Reflektion für die erste Ordnung bestimmen.
\begin{align}
    \lambda_{gr} &= 2d\sin(\theta_{gr}) \\
    \Delta \lambda_{gr} &= 2d \cos(\theta_{gr})  \theta_{gr}.
\end{align}

Laut Skript ist $d = \SI{201.4}{pm}$.
Somit erhalten wir die Grenzwellenlänge
\begin{equation}
    \boxed{\lambda_{gr} = \SI{3.371(3)E-11}{m}}
\end{equation}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/grenzwinkel_LiF.png}
    \caption{Grenzwinkelbestimmung 1.Ordnung LiF}%
    \label{fig:grenzwinkel_lif}
\end{figure}


\subsubsection{Bestimmung des Planck'schen Wirkungsquantum $h$}%
\label{ssub:bestimmung_des_planck_schen_wirkungsquantum_h_}

Aus Gleichung \eqref{eq:grenz_lambda} können wir nun das Planck'sche Wirkumsquantum bestimmen:
\begin{align}
    h &= \frac{eU\lambda_{gr}}{c}  \\
    \Delta h &= h \cdot \sqrt{\left( \frac{\Delta\lambda_{gr}}{\lambda_{gr}} \right)^2 + \left( \frac{\Delta U}{U} \right)^2}
\end{align}
Die Röhrenspannung $U = \SI{35.00(5)}{V}$ mit Ablesefehler. Für $c$ und  $e$ wurden die Literaturwerte verwendet.

\begin{equation}
    \boxed{h = \SI{6.30(23)E-34}{\joule\second}}
\end{equation}

Der Wert weicht $1.35\sigma$ vom Literaturwert  $h^{lit} = \SI{6.626070E-34}{Js}$ ab.
Die Abweichung ist daher nicht signifikant.
Der relative Fehler des Messwerts beträgt $3.6\%$.
Angesichts der Einfachheit des Experiments und der kurzen Messzeit ist der Fehler akzeptabel.
Die Schwankung des Untergrunds  $n_0$ ist gegenüber dem Fehler in den Fitparametern vernachlässigbar.

\subsubsection{Grenzwinkel des Spektrums 2. Ordnung}%
\label{ssub:grenzwinkel_des_spektrums_2_ordnung}

Zur Bestimmung des Grenzwinkels 2. Ordnung $\theta_{gr,2}$ für gegebene Grenzwellenlänge $\lambda_{gr}$ verwenden wir erneut die Bragg-Reflektion nach Gleichung \eqref{eq:bragg}.
In diesem Fall für $n=2$.
 \begin{align}
     \theta_{gr,2} &= \arcsin\left( \frac{\lambda_{gr}}{d} \right) \\
     \Delta\theta_{gr,2} &= \frac{1}{d\sqrt{1-\left(\frac{\lambda_gr}{d}\right)^2}} \Delta\lambda_{gr}
\end{align}
\begin{equation}
    \boxed{\theta_{gr,2} = \SI{9.6(5)}{\degree}}
\end{equation}

Das Spektrum zweiter Ordnung fällt im Röntgenspektrum nicht auf, da es von dem Spektrum 1. Ordnung sowie der $K_{\alpha}$-Linie 1. Ordnung überdeckt wird.

\subsection{Bestimmung der $K_{alpha}$- und $K_{beta}$-Linien}
\label{sub:bestimmung_der_k__alpha_und_k__beta_linien}

In Abbildung \ref{fig:Kb_1ord} bis \ref{fig:Ka_2ord} ist die Zählrate gegen den Winkel für die Bereiche der $K_{\alpha}$- und $K_{\beta}$-Linien aufgetragen.
An die Messwerte wurde eine Gaußverteilung gefittet.
Der Mittelwert $\mu$ der Verteilung entspricht gerade dem der K-Linie zugehörigen Winkel.
Der Fehler ist gerade durch die Standardabweichung $\sigma$ gegeben.
Die bestimmten Werte sind in Tabelle \ref{tab:k_linie_LiF} aufgelistet.

\begin{table}[htpb]
   \centering
   \caption{Winkelbestimmung der $K_{\alpha}$- und $K_{\beta}$-Linien LiF}
   \label{tab:k_linie_LiF}
   \begin{tabular}{cS[table-format = 2.2(2)]}
    \toprule
    Linie & {$\mu$ [$\si{\degree}$]} \\ \midrule
    $K_{\beta}$, 1. Ordnung  & 8.9(1) \\
    $K_{\alpha}$, 1. Ordnung & 10.06(11) \\
    $K_{\beta}$, 2. Ordnung  & 18.17(12) \\
    $K_{\alpha}$, 2. Ordnung & 20.5(1) \\ \bottomrule
   \end{tabular}
\end{table}

Die korrespondieren Winkel werden Analog wie zuvor durchgeführt.
Die Werte der entsprechenden Wellenlängen sind in Tabelle \ref{tab:k_linie_lambda_LiF} aufgeführt.

\begin{table}[htpb]
   \centering
   \caption{Winkelbestimmung der $K_{\alpha}$- und $K_{\beta}$-Linien LiF}
   \label{tab:k_linie_lambda_LiF}
   \begin{tabular}{cS[table-format = 2.2(2)]}
    \toprule
    Linie & {$\lambda$ [$\si{m}$]} \\ \midrule
    $K_{\beta}$, 1. Ordnung  & 6.23(8) \\
    $K_{\alpha}$, 1. Ordnung & 7.04(8) \\
    $K_{\beta}$, 2. Ordnung  & 6.28(4) \\
    $K_{\alpha}$, 2. Ordnung & 7.06(4) \\ \bottomrule
   \end{tabular}
\end{table}

Zum Vergleich müssen wir die  $K_{\alpha}$- und $K_{\beta}$-Linien noch mitteln.
\begin{align}
    \bar{\lambda_{i}} &= \frac{\lambda_{i,1} + \lambda_{i,2}}{2} \\
    \Delta\bar{\lambda_{i}} &= \frac{1}{2} \sqrt{\Delta\lambda_{i,1}^2 + \Delta\lambda_{i,2}^2}
\end{align}

Die $K$-Linien von Molybdän sind demnach
\begin{align}
    \bar{\lambda_{\alpha}}  &= \SI{7.05(4)E-11}{m} \\
    \bar{\lambda_{\beta}}   &= \SI{6.26(4)E-11}{m}
\end{align}

Die $K_{\alpha}$-Linie weicht $1.35\sigma$ vom Literaturwert $K_{\alpha}^{lit} = \SI{71.1}{pm}$ ab.
Die $K_{\beta}$-Linie weicht $1.18\sigma$ vom Literaturwert $K_{\beta}^{lit} = \SI{63.1}{pm}$ ab.
Beide Abweichungen sind dementsprechend nicht signifikant.
Der relative Fehler der $K_{\alpha}$-Linie und der $K_{\beta}$-Linie beträgt jeweils 0.6\%.

\subsubsection{Halbwertsbreite}%
\label{ssub:halbwertsbreite}

Die Halbwertsbreite wurde für die $K_{\alpha}$-Linie erster Ordnung bestimmt.
Mit gegebener Standardabweichung $\sigma$ der gefitteten Gaussverteilung ergibt sich
\begin{align}
    H_{1/2} &= 2\sqrt{2\ln 2}  \sigma \\
    \Delta H_{1/2} &= 2\sqrt{2\ln 2}  \Delta\sigma.
\end{align}

Die Halbwertsbreite beträgt somit
\begin{equation}
    \boxed{H_{1/2} = \SI{0.260(5)}{\degree}}
\end{equation}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/Kb_1ord.png}
    \caption{$K_{\beta}$-Linie 1. Ordnung LiF}%
    \label{fig:Kb_1ord}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/Ka_1ord.png}
    \caption{$K_{\alpha}$-Linie 1. Ordnung LiF}%
    \label{fig:Ka_1ord}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/Kb_2ord.png}
    \caption{$K_{\beta}$-Linie 2. Ordnung LiF}%
    \label{fig:Kb_2ord}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/Ka_2ord.png}
    \caption{$K_{\alpha}$-Linie 2. Ordnung LiF}%
    \label{fig:Ka_2ord}
\end{figure}

\subsection{Isochromatenmethode zur Bestimmung von $h$}
\label{sub:isochromatenmethode_zur_bestimmung_von_h_}

Um die Einsatzspannung $U_{E}$ zu bestimmen tragen wir die Zählrate gegen die Spannung auf.
Die Untergrundzählrate wird aus den ersten drei Werten ($\SI{20}{kv} - \SI{22}{kV}$) als Mittelwert bestimmt.
Der Fehler ist die Standardabweichung der drei Werte.
\begin{equation}
    \boxed{n_0 = \SI{2.2(8)}{1/s}}
\end{equation}

Für den linearen Fit erhalten wir als Fitparameter
\begin{align}
    m &= \SI{33.4(2)}{kV/s}  \\
    b &= \SI{-750(4)}{1/s}
\end{align}

Die Messwerte als auch die Fitfunktion sind in Abbildung \ref{fig:einsatzspannung} dargestellt.

Ähnlich wie zuvor bestimmen wir den Schnittpunkt mit der Untergrundzählrate und erhalten für die Einsatzspannung
\begin{equation}
    \boxed{U_{E} = \SI{22.51(7)}{kV}}
\end{equation}

Das Planck'sche Wirkumsquantum bestimmen wir ebenfalls analog wie zuvor bei Winkel $\beta = \SI{7.50(5)}{\degree}$.
\begin{align}
    h &= \frac{\lambda_{gr} e U_{E}}{c} = \frac{eU_{E}2d\sin(\beta)}{c} \\
    \Delta h &= h \sqrt{\left( \frac{\Delta U_{E}}{U_{E}} \right)^2 + \left( \frac{\Delta \beta}{\tan(\beta)} \right)^2}
\end{align}

und somit
\begin{equation}
    \boxed{h = \SI{6.33(5)E-34}{Js}}
\end{equation}

Der Wert weicht $6.45\sigma$ von dem Literaturwert  $h = \SI{6.62607E-34}{Js}$ ab.
Der relative Fehler in dem gemessenen Wert beträgt 0.8\%.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/einsatzspannung.png}
    \caption{Bestimmung der Einsatzspannung}%
    \label{fig:einsatzspannung}
\end{figure}

\subsection{Messung des Röntgenspektrums eines NaCl-Kristalls}%
\label{sub:messung_des_rontgenspektrums_eines_nacl_kristalls}

Zuletzt werten wir noch das Röntgenspektrum von NaCl aus.
Das Spektrum wurde im Bereich von $3\si{\degree}$ bis  $14\si{\degree}$ aufgenommen. In Abbildung \ref{fig:mess_nacl} sind die Messdaten dargestellt. In Abbildung \ref{fig:k_linie1_nacl} und \ref{fig:k_linie2_nacl} wurde an die Daten erneut eine Gausskurve gefittet um die $K_{\alpha}$- und $K_{\beta}$-Linien 1. und 2. Ordnung zu bestimmen.


\begin{table}[htpb]
   \centering
   \caption{Winkelbestimmung der $K_{\alpha}$- und $K_{\beta}$-Linien NaCl}
   \label{tab:k_linie_NaCl}
   \begin{tabular}{cS[table-format = 2.2(2)]}
    \toprule
    Linie & {$\mu$ [$\si{\degree}$]} \\ \midrule
    $K_{\beta}$, 1. Ordnung  & 6.4(1) \\
    $K_{\alpha}$, 1. Ordnung & 7.2(1) \\
    $K_{\beta}$, 2. Ordnung  & 12.9(1) \\
    $K_{\alpha}$, 2. Ordnung & 14.6(1) \\ \bottomrule
   \end{tabular}
\end{table}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/mess_NaCl.png}
    \caption{Röntgenspektrum NaCl}%
    \label{fig:mess_nacl}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/1ord_nacl.png}
    \caption{$K_{\alpha}$- und $K_{\beta}$-Linie 1. Ordnung NaCl}%
    \label{fig:k_linie1_nacl}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/2ord_nacl.png}
    \caption{$K_{\alpha}$- und $K_{\beta}$-Linie 2. Ordnung NaCl}%
    \label{fig:k_linie2_nacl}
\end{figure}


Da die Werte nur durch wenige Punkte bestimmt wurden sind die Werte nur als gute Indikatoren zu verstehen.

Der Netzebenen Abstand lässt sich nun erneut aus Gleichung \eqref{eq:bragg} bestimmen.
\begin{align}
    d &= \frac{n\lambda}{2\sin\theta} \\
    \Delta d &= d \cdot \sqrt{\left( \frac{\Delta \lambda}{\lambda} \right)^2 + \left( \frac{\Delta \theta}{\theta} \right)^2}
\end{align}

\begin{table}[htpb]
   \centering
   \caption{Bestimmung des Netzabstands aus $K_{\alpha}$- und $K_{\beta}$-Linien NaCl}
   \label{tab:netz_NaCl}
   \begin{tabular}{cS[table-format = 2.2(2)E2]}
    \toprule
    Linie & {$d$ [$\si{m}$]} \\ \midrule
    $K_{\beta}$, 1. Ordnung  & 2.80(5)E-10 \\
    $K_{\alpha}$, 1. Ordnung & 2.80(5)E-10 \\
    $K_{\beta}$, 2. Ordnung  & 2.79(4)E-10 \\
    $K_{\alpha}$, 2. Ordnung & 2.80(3)E-10 \\ \bottomrule
   \end{tabular}
\end{table}

Wir mitteln und erhalten den Mittelwert $\bar{d}$.
Der statistische Fehler entspricht gerade der Standardabweichung der Werte.
Der systematische Fehler ergibt sich aus der Gauss'schen Fehlerfortpflanzung.
Somit erhalten wir
 \begin{equation}
     \boxed{\bar{d} = (280 \pm 0.2 \text{ stat. } \pm 2 \text{ sys.})\text{pm}}
\end{equation}

Für die Gitterkonstante ergibt sich aus $a = 2d$:
\begin{equation}
    \boxed{a = \SI{560(4)}{pm}}
\end{equation}

\subsubsection{Avogadro Konstante}%
\label{ssub:avogadro_konstante}

Zuletzt bestimmen wir noch die Avogadro-Konstante nach Gleichung \eqref{eq:avo}.
\begin{align}
    N_{A } &= \frac{1}{2} \frac{M_{Mol}}{\rho} \frac{1}{d^3} \\
    \Delta N_{A} &= 3 N_{A} \frac{\Delta d}{d}
\end{align}

Die Werte der molaren Masse $M_{Mol} = \SI{58.44}{g/mol}$ und der Dichte $\rho = \SI{2.154}{g/cm^3}$ von NaCl entnehmen wir dem Skript.

Die Avogadro-Konstante bestimmen wir dann zu
\begin{equation}
    \boxed{N_{A} = \SI{6.17(14)E23}{1/mol}}
\end{equation}

Der Wert weicht gerade $1.02\sigma$ vom Literaturwert  $N_{A}^{lit} = \SI{6.0221E23}{1/mol}$ ab.
Der gemessene Wert stimmt trotz eines geringen relativen Fehlers von 2\% gut mit dem Literaturwert überein.

\section{Zusammenfassung und Diskussion}%
\label{sec:zusammenfassung_und_diskussion}

In diesem Versuch haben wir die Röntgenspektra von LiF und NaCl ausgewertet.
Für LiF konnten wir dabei das Planck'sche Wirkumsquantum $h$ auf zwei Weisen bestimmen.
Weiterhin konnten wir die K-Linien von Molybdän messen.
Anschließend konnten wir aus dem Röntgenspektrum von NaCl den Netzebenenabstand sowie die Avogadro-Zahl bestimmen. Wir erhielten dabei folgende Messergebnisse.
\begin{align}
    \text{Spektrumsextrapolation: }&  h &= \SI{6.30(23)E-34}{Js} \\
    \text{Isochromatenmethode: }&  h &= \SI{6.33(5)E-34}{Js}
\end{align}
\begin{align}
    \lambda_{\alpha} &= \SI{7.05(4)E-11}{m} \\
    \lambda_{\beta} &= \SI{6.26(4)E-11}{m} \\
\end{align}
Halbwertsbreite der $K_{\alpha}$-Linie 1. Ordnung von LiF:
\begin{equation}
    H_{1/2} = \SI{0.260(5)}{\degree}
\end{equation}
\begin{align}
    \bar{d} &= (280 \pm 0.2 \text{ stat. } \pm 2 \text{ sys.})\text{ pm} \\
    N_{A} &= \SI{6.17(14)E23}{1/mol}
\end{align}

Alle Werte, bis auf $h$ aus der Isochromatenmethode, stimmen im $1.5\sigma$ Bereich mit den Literaturwerten überein.
Theoretisch sollte die Isochromatenmethode ein genaueres Ergebnis liefern.
Die Abweichung ist mit $6.45\sigma$ signifikant.
Die Isochromatenmethode ist deshalb theoretisch genauer, weil bei der Messung keine Korrekturen für die Eigenabsorption der Anode und des Röhrenfensters beachtet werden müssen.
Es ist davon auszugehen, dass daher systematische Fehler vorliegen.
Im Fall der Spektrumsextrapolation ist der relative Fehler eine Größenordnung größer als im Fall der Isochromatenmethode.
Dies liegt vor allem an dem großen Fehler in der Schnittstellenbestimmung.
Evtl. heben sich unterschiedliche systematische Fehler auch gegenseitig auf.
In Abbildung \ref{fig:einsatzspannung} ist zu erkennen, dass für große Spannungen die Messwerte leicht von der Gerade abweichen.
Wären die späteren Werte verworfen worden wäre der systematische Fehler eventuell kleiner.

%%% Notebook BEGIN

\includepdf[addtotoc={1,section,2,Notebook,notebook},frame=true,pages=-]{data/versuch255.pdf}

%%% Notebook END

\end{document}

%%% TODO
