%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
\documentclass{../resources/pap2}


\setcounter{table}{1}
\setcounter{versuchnr}{251}
\renewcommand{\versuchname}{Statistik}

\usepackage{fontspec}
\usepackage{polyglossia}
\setdefaultlanguage{german}
\newcommand{\figref}[1]{Abbildung \ref{#1}}
\newcommand{\equref}[1]{Gleichung \eqref{#1}}
\newcommand{\tabref}[1]{Tabelle \eqref{#1}}


\begin{document}
%%% Titelseite BEGIN

\thispagestyle{firstpage}
\newgeometry{top=0.5in+\voffset+\headheight}
\protokollheader
\tableofcontents
\restoregeometry

%%% Titelseite END

%%% Messprotokoll BEGIN

\includepdf[addtotoc={1,section,2,Messprotokoll,messprotokoll},frame=true,pages=-]{data/messprotokoll.pdf}

%%% Messprotokoll END

\section{Einleitung}
Im Versuch der 251 \textit{Statistik} beschäftigen wir uns mit dem radioaktiven Zerfall und seiner statistischen Beschreibung. Dabei vermessen wir die Zählrohrcharakteristik eines des Geiger-Müller-Zählrohrs sowie das Plateau in der Kennlinie des Zählrohres. Weiterhin untersuchen wir die Schwankungen in der Zählrate experimentell und bestätigen dabei die statistische Natur des radioaktiven Zerfalls. Wir untersuchen die Schwankungen durch Vergleich mit der Gauss- und Poissonverteilung.

\section{Grundlagen}
Der Zerfallsprozess eines radioaktiven Teilchens ist zufällig und unabhängig von dem anderer Teilchen. Bei der Zerfallsmessung mehrere Teilchen über einen Zeitraum $t$ wird die Zahl der zerfallenen Teilchen variieren. Wenn viele solcher Einzelmessungen ausgewertet werden erhält man eine Verteilung der Zerfallsrate.

\subsection{Binomial-Verteilung}%
\label{sub:binomial_verteilung}

Die Binomial-Verteilung beschreibt gerade die Wahrscheinlichkeit, dass ein Ereignis  $A$ bei  $n$ unabhängigen Versuchen $k$ mal eintritt, wenn die $p$ die Wahrscheinlichkeit ist, dass das Ergebnis $A$ in einer Einzelmessung eintritt und $1-p$ die Wahrscheinlichkeit ist, dass es nicht eintritt. Da die Reihenfolge in welcher $A$ eintritt bzw. nicht eintritt keine Rolle spielt ergibt sich für die Binomial-Verteilung
\begin{equation}
    B(k;n,p) = \binom{n}{k} p^{k}(1-p)^{n-k}
\end{equation}
Die Binomialverteilung ist eine diskrete Verteilung, die durch die zwei Parameter $n$ und $p$ vollständig beschrieben wird. Weiterhin gelten folgende Eigenschaften.
\begin{align}
    &\text{Normierung:}         & \qquad &  &                   & \sum_{k=0}^{n} B(k;n,p)=1 \\
    &\text{Mittelwert}          & \qquad &  &\langle k \rangle =& \sum_{k=0}^{n} k B(k;n,p) = np \\
    &\text{Varianz:}            & \qquad &  &\sigma^2          =& \sum_{k=0}^{n} k^2B(k;n,p) - \langle k \rangle^2 = np(1-p) \\
    &\text{Standardabweichung:} & \qquad &  &\sigma            =& \sqrt{np(1-p)}
\end{align}

\subsection{Radioaktiver Zerfall}%
\label{sub:radioaktiver_zerfall}

Die Zerfallswahrscheinlichkeit eines einzelnen radioaktiven Teilchens $p$ ist abhängig von dem gewählten Beobachtungszeitraum:
\begin{equation}
    p(t) = 1 - e^{-\lambda t}.
\end{equation}
Dabei ist $\lambda$ eine für ein Isotop charakteristische Größe. Ist die Zerfallskonstante klein gegenüber dem Beobachtungszeitraum, so kann die Wahrscheinlichkeit als konstant angenommen werden und die Binomialverteilung beschreibt die Verteilung von Einzelmessungen eines Präparats.

\subsection{Poisson-Verteilung}%
\label{sub:poisson_verteilung}
Für sehr große $n$ wird die Binomialverteilung unter anderem aufgrund der involvierten Fakultäten schwer zu berechnen. In dem Fall, dass allerdings die Zerfallsgeschwindigkeit $p \rightarrow 0 $ sehr klein und die Anzahl radioaktiver Teilchen sehr groß $n \rightarrow \infty$ wird lässt sich die Binomial-Verteilung durch die Poisson-Verteilung approximieren.
\begin{equation}
    P(k;\mu) = \frac{\mu^{k}e^{-\mu}}{k!}
\end{equation}

Die Poisson-Verteilung ist wie die Binomial-Verteilung eine diskrete Verteilung ($k\in\mathbb{N}$). Sie ist allerdings vollständig durch den Mittelwert $\mu$ parametrisiert. Für die Binomial-Verteilung gelten die Eigenschaften:
\begin{align}
    &\text{Normierung:}         & \qquad &  &                   & \sum_{k=0}^{\infty} P(k;\mu)=1 \\
    &\text{Mittelwert}          & \qquad &  &\langle k \rangle =& \sum_{k=0}^{\infty} k P(k;\mu) = \mu \\
    &\text{Varianz:}            & \qquad &  &\sigma^2          =& \sum_{k=0}^{\infty} k^2P(k;\mu) - \langle k \rangle^2 = \mu \\
    &\text{Standardabweichung:} & \qquad &  &\sigma            =& \sqrt{\mu}
\end{align}

\subsection{Gauss-Verteilung}%
\label{sub:gauss_verteilung}
Die Gauss-Verteilung nähert die Poisson-Verteilung für große Mittelwerte ($\mu>30$) an.
\begin{equation}
    G(k;\mu) = \frac{1}{\sqrt{2\pi}\sigma} e^{-\frac{(\mu-k)^2}{2\sigma^2}}
\end{equation}
Wobei im Fall, dass wie im Fall der Poisson Verteilung $\mu = \sigma^2$ gilt, die Gauss-Verteilung sich vereinfacht zu.
\begin{equation}
    G(k;\mu) = \frac{1}{\sqrt{2\pi\mu}} e^{-\frac{(\mu-k)^2}{2\mu}}
    \label{eq:zählstatistik}
\end{equation}
Die Gauss-Verteilung besitzt die Eigenschaften:
\begin{align}
    &\text{Normierung:}         & \qquad &  &                   & \int_{-\infty}^{\infty} G(k;\mu,\sigma)dk=1 \\
    &\text{Mittelwert}          & \qquad &  &\mu               =& \int_{-\infty}^{\infty} k G(k;\mu,\sigma)dk \\
    &\text{Varianz:}            & \qquad &  &\sigma^2          =& \int_{-\infty}^{\infty} k^2G(k;\mu,\sigma)dk - \langle k \rangle^2
\end{align}

Die Gauss-Verteilung ist kontinuierlich und sowohl durch die Standardabweichung $\sigma$ und den Mittelwert $\mu$ parametrisiert.

Die Wahrscheinlichkeit, dass ein gemessener Wert mehr als $\alpha\sigma$, wobei $\alpha \in \mathbb{R}$, abweicht ist gegeben durch
\begin{equation}
    P_{\alpha\sigma} = 1 - \int_{\mu-\alpha\sigma}^{\mu+\alpha\sigma} G(k;\mu,\sigma)dk
\end{equation}
gegeben.

Da die Wahrscheinlichkeit, dass eine Einzelmessung mehr als $1\sigma$ von $\mu$ abweicht 31.73\% beträgt lässt sich der Mittelwert in der Praxis gut durch den Wert einer Einzelmessung $k$ schätzen.

\section{Durchführung}
\subsection{Untersuchung des Plateauanstiegs}%
\label{sub:untersuchung_des_plateauanstiegs}

Wir messen die Zählrate jeweils 1 Minute bzw. 3 Minuten lang bei den Spannungen $U_0$ und $U_0 + \SI{100}{\volt}$.

\subsection{Verifizierung der statistischen Natur des radioaktiven Zerfalls}%
\label{sub:verifizierung_der_statistischen_natur_des_radioaktiven_zerfalls}

Wir bringen das Präparat in die Nähe des Zählrohrs sodass die Zählrate unabhängig von der Totzeit ist. Danach nehmen wir 2000 Einzelmessungen über eine Zeitraum von ca. $\SI{500}{\milli\second}$ von der Anzahl der Zerfälle.

\subsection{Vergleich der Poisson- und Gauss- Verteilung bei sehr kleinen Zählraten}%
\label{sub:vergleich_der_poisson_und_gauss_verteilung_bei_sehr_kleinen_zahlraten}

Wir führen die selbe Messung wie zuvor vor. Allerdings entfernen wir das Präparat weiter vom Zählrohr und wählen einen kürzeren Messzeitraum. Dieses Mal nehmen wir 5000 Einzelmessungen auf.

\section{Auswertung}
\subsection{Zählrohrcharakteristik}%
\label{sub:zahlrohrcharakteristik}

In Abbildung \ref{fig:zaehlrohrcharakteristik} ist die Anzahl der Ereignisse in Abhängigkeit der Zählrohrspannung aufgetragen.
Die gefittete Gerade hat eine Steigung von
\begin{equation}
    \boxed{a = \SI{0.6(1)}{\text{Ereignisse}/\volt}}.
\end{equation}

Über einen Spannungsbereich von $\SI{100}{\volt}$ Ändert sich die Anzahl gemessener Ereignisse um ca. 2.8\%. Die Zählrate kann daher in guter Näherung in diesem Spannungsbereich als konstant angenommen werden.

\subsection{Plateaubereich des Zählrohrs}%
\label{sub:plateaubereich_des_zahlrohrs}

Im folgenden untersuchen wir den Anstieg des Plateaubereichs.
Der Anstieg ist dabei die Differenz aus der Messung bei $U_0$ und $U_0 + \SI{100}{\volt}$
\begin{equation}
    \text{PA} = N(U_0) - N(U_0 + \SI{100}{\volt}), \qquad \Delta \text{PA} = \sqrt{N(U_0) + N(U_0 + \SI{100}{\volt})}
\end{equation}
Mit einem relativen Anstieg
\begin{equation}
    a = \frac{\text{PA}}{N(U_0)}, \qquad \Delta  a =  \frac{\text{PA}}{N(U_0)} \sqrt{\left( \frac{\Delta \text{PA}}{PA} \right)^2 + \left( \frac{\sqrt{N(U_0)}}{N(U_0)} \right)^2}
\end{equation}
Die Ergebnisse sind in Tabelle \ref{tab:plateaubereich} zu finden. \\

Weiterhin können wir die Zeit bestimmen, die wir messen müssten um den Plateauanstieg auf 1\% genau zu kennen.
Angenommen der prozentuale Fehler des Anstiegs ist linear von der Zeit abhängig, so können wir den Fehler des prozentualen Anstieg $\Delta a$ wie folgt beschreiben

\begin{equation}
    \Delta a = \frac{\Delta a(t_1)-\Delta a(t_0)}{t_1 - t_0} t + 1
\end{equation}

und somit beträgt die benötigte Zeit
\begin{equation}
    t = (1\%-1) \frac{t_1 - t_0}{\Delta a(t_1)-\Delta a(t_0)}
\end{equation}
nach den Ergebnisse aus Tabelle \ref{tab:plateaubereich} brauchen wir somit
\begin{equation}
    \boxed{t = \SI{21.91}{\minute}}
\end{equation}
damit der Fehler kleiner 1\% wird. \\

Zuletzt bestimmen wir noch welche prozentual Variation der Zählrate bei einer Spannungserhöhung um $\SI{100}{\volt}$ möglich bei einem Vertrauensniveau von $1\sigma$ bzw. $2\sigma$ ist.

Allgemein für $n\sigma$ ist diese Gegeben durch
\begin{equation}
    \frac{n\sqrt{N(U_0)}}{N(U_0)} = \frac{n}{\sqrt{N(U_0)}}
\end{equation}
Die Ergebnisse sind erneut Tabelle \ref{tab:plateaubereich} zu entnehmen.

\begin{table}
    \centering
    \caption{Auswertungsergebnisse Plateauanstieg}
    \begin{tabular}{l@{}@{}cc}
    \toprule
     & $t = \SI{1}{\minute}$ & $t = \SI{3}{\minute}$ \\ \toprule
    PA & $\SI{210(180)}{1/60\volt}$ & $\SI{420(310)}{1/180\volt}$ \\ \midrule
    $a$ & $\SI{0.013(11)}{1/100\volt}$ & $\SI{0.0086(65)}{1/100\volt}$ \\ \midrule
    Variation $1\sigma$ & 0.79\% & 0.46\% \\ \midrule
    Variation $2\sigma$ & 1.59\% & 0.91\% \\ \bottomrule
\end{tabular}
\label{tab:plateaubereich}
\end{table}



\begin{figure}[htb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/zaehlrohrcharakteristik.jpg}
    \caption{Zählrohrcharakteristik}%
    \label{fig:zaehlrohrcharakteristik}
\end{figure}

\subsection{Auswertung der Daten mit hoher mittlerer Ereigniszahl}%
\label{sub:auswertung_der_daten_mit_hoher_mittlerer_ereigniszahl}

In Abbildung \ref{fig:verteilung_hohe_mittlere_verteilungszahl} sind die Messwerte der Messung bei hohere mittlerer Ereigniszahl dargestellt.
Es wurde eine Gauss- und eine Poisson-Verteilung zu den Daten gefittet.
Es ist erkennbar, dass die Messwerte gut sowohl mit der Poisson als auch der Gauss-Verteilung übereinstimmen. Die Fitparameter betragen:
\begin{table}[htbp]
\centering
\caption{Fitparameter: Gauss- und Poisson-Verteilung bei hoher mittlerer Ereigniszahl}
\begin{tabular}{ccc}
   \toprule
     & Gauss & Poisson \\ \toprule
    $A$ & $\SI{3885(82)}{}$ & $\SI{3870(73)}{}$ \\ \midrule
    $\mu$ & $\SI{74.0(2)}{1/\second}{}$ & $\SI{74.23(18)}{1/\second}$ \\ \midrule
    $\sigma$ & $\SI{8.6(2)}{1/\second}$ & - \\ \bottomrule
\end{tabular}
\label{tab:fitparameter_hohe_mittlere_ereigniszahl}
\end{table}

Um die Güte des Fits zu bewerten betrachten wir im folgenden die $\chi^2$ Abweichungen
\begin{table}[htbp]
    \centering
    \caption{Güte der Fits: Gauss- und Poissonverteilung bei hoher mittlerer Ereigniszahl}
    \label{tab:guete_hohe_mittlere_ereigniszahl}
    \begin{tabular}{lp{3cm}p{3cm}}
    \toprule
     & Gauss & Poisson \\ \toprule
    $\chi^2$ & 42.3 & 40.5 \\ \midrule
    $\chi_{red}^2$ & 1.36 & 1.26 \\ \midrule
    Wahrscheinlichkeit & 8.0\% & 15.0\% \\
    \bottomrule
\end{tabular}
\end{table}

Aus den $\chi^2$ Werten schließen wir, dass die Fits nicht ideal sind.
Dabei wurde bereits, wie in \textit{\ref{notebook} \nameref{notebook}} zu sehen ist, Randpunkte vernachlässigt.
Wahrscheinlich wäre es nötig weitere Daten aufzunehmen um zu bestätigen, dass es sich tatsächlich um eine Gauss- bzw. Poisson-Verteilung handelt.
Bisher ist die Poisson-Verteilung eine bessere Approximation der Messdaten, wenn auch nicht allzu deutlich.
Beide Verteilungen weichen nicht stark von den Messwerten ab.
Die Ergebnisse widersprechen zumindest nicht der Theorie, dass für große Mittelwerte $\mu > 74$ die Poisson-Verteilung in eine Gauss-Verteilung übergeht.
Ein weiteres Indiz dafür ist, dass die Standardabweichung der Gaussverteilung nur geringfügig von der Quadratwurzel des Mittelwerts abweicht ($\delta = 8.6 = \sqrt{\mu}$)
Wie sich dies für kleine Mittelwerte verhält werden wir im folgenden Abschnitt sehen.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/verteilung_hohe_mittlere_verteilungszahl.jpg}
    \caption{Verteilung hohe mittlere Verteilungszahl}%
    \label{fig:verteilung_hohe_mittlere_verteilungszahl}
\end{figure}

\subsection{Auswertung der Daten mit kleiner mittlerer Ereigniszahl}%
\label{sub:auswertung_der_daten_mit_kleiner_mittlerer_ereigniszahl}

Wir verfahren genauso wie in \textit{\ref{sub:auswertung_der_daten_mit_hoher_mittlerer_ereigniszahl} \nameref{sub:auswertung_der_daten_mit_hoher_mittlerer_ereigniszahl}}.
Die Messdaten und die gefitteten Gauss- und Poisson-Verteilung sind in %\figref{fig:verteilung_kleine_mittlere_verteilungszahl} dargestellt. Die Fitparameter sind in

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/verteilung_kleine_mittlere_verteilungszahl.jpg}
    \caption{Verteilung Kleine Mittlere Verteilungszahl}%
    \label{fig:verteilung_kleine_mittlere_verteilungszahl}
\end{figure}

\begin{table}[htbp]
    \centering
    \caption{Fitparameter: Gauss- und Poisson-Verteilung bei kleiner mittlerer Ereigniszahl}
    \label{tab:fitparameter_kleine_mittlere_ereigniszahl}
    \begin{tabular}{ccc}
    \toprule
     & Gauss & Poisson \\ \toprule
    $A$ & $\SI{3600(200)}{}$ & $\SI{3700(80)}{}$ \\ \midrule
    $\mu$ & $\SI{5.3(1)}{1/\second}{}$ & $\SI{5.32(5)}{1/\second}$ \\ \midrule
    $\sigma$ & $\SI{2.14(9)}{1/\second}$ & - \\ \bottomrule
    \end{tabular}
\end{table}

\begin{table}[htbp]
    \centering
    \caption{Güte der Fits: Gauss- und Poissonverteilung bei kleiner mittlerer Ereigniszahl}
    \label{tab:guete_kleine_mittlere_ereigniszahl}
    \begin{tabular}{lp{3cm}p{3cm}}
    \toprule
                        & Gauss     & Poisson   \\ \toprule
    $\chi^2$            & 119.2     & 18.2      \\ \midrule
    $\chi_{red}^2$      & 11.9      & 1.65      \\ \midrule
    Wahrscheinlichkeit  & 0.0\%     & 8.0\%     \\
    \bottomrule
    \end{tabular}
\end{table}

Es ist ersichtlich, dass die Poisson-Verteilung die Daten besser abbildet als die Gauss-Verteilung ($\chi_{g,red}^2 \gg \chi_{p,red}^2$).
Auch ist der Fehler in den Fitparameter der Gauss-Verteilung deutliche größer.
Allerdings ist auch hier ist die Poisson-Verteilung noch nicht als Beschreibung ideal und weicht 0.65 vom idealen $\chi_{red}^2 = 1$ ab.
Wahrscheinlich würde sich ein anderes Bild ergeben, wenn noch mehr Daten aufgenommen werden.
Vor allem für große Werte wird die Häufigkeit dann näher an der statistichen Häufigkeit liegen.
Wir können also feststellen, dass für kleine Mittelwerte ($\mu \approx 5$) die Häufigkeit sich nicht entsprechend einer Gauss-Verteilung verteilt.

Alle Rechnungen können im Detail ebenfalls in \textit{\ref{notebook} \nameref{notebook}} nachvollzogen werden.

\clearpage
\section{Zusammenfassung und Diskussion}
In Versuch 251 \textit{Statistik} haben wir uns mit der statistischen Natur des radioaktiven Zerfalls beschäftigt.
Zunöchst haben wir das Plateau des verwendeten Zählrohrs untersucht.
Hierbei konnten wir feststellen, dass die Zahl registriereter Zerfälle für geringe Torzeiten ($t\approx \SI{30}{\second}$) im Plateaubereich nicht signifikant zunimmt.
\begin{equation}
    \boxed{a = \SI{0.6(1)}{\text{Ereignisse}/\volt}}.
\end{equation}
Für größere Torzeiten ($\SI{1}{\minute}$, $\SI{3}{\minute}$) ist die Spannungsänderung dabei durchaus von Bedeutung.

Im folgenden haben wir die charakteristische Statistik des radioaktiven Zerfalls untersucht indem wir Gauss- und Poisson-Verteilungen an die Messwerte angepasst haben.
Für große Mittelwerte konnten wir dabei sowohl für die Gauss- als auch Poisson-Verteilung eine nicht vernachlässigbare Übereinstimmung mit den Messwerten feststellen.
Weiterhin konnten wir feststellen, dass die Poisson- und Gauss-Verteilung in diesem Fall gut mit einander übereinstimmten.
Für kleine Mittelwerte konnten wir dies allerdings nicht feststellen. Hier war die Poisson-Verteilung deutlich näher an den Messwerten.
Sowohl für kleine und große Mittelwerte waren die Verteilungen allerdings nicht ideal.
Der größte "Fehler" in der Messung war dabei die Anzahl der Messungen.
Eine Verdopplung der Anzahl der Messwerte hätte vor allem für Messwerte weiter entfernt vom Mittelwert zu Ergebnissen geführt, die besser mit der Theorie übereinstimmen.
Bei den systematischen Fehlern ist davon auszugehen, dass diese venachlässig bar sind.

%%% Notebook BEGIN

\includepdf[addtotoc={1,section,2,Notebook,notebook},frame=false,pages=-]{data/versuch251.pdf}

%%% Notebook END


%%% Anhang
% \section{Anhang}
% \begin{figure}
%     \centering
%     \includegraphics[width=0.8\textwidth]{~/.vim/UltiSnips/proxyimage}
%     \caption{}
%     \label{fig:}
% \end{figure}


\end{document}

%%% TODO: aktualisiere notebook
